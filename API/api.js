const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const fs = require('fs');
const app = express();

const port = process.env.PORT || 5000;
const CloudManager = require('./manager');

//Setting Cross-Origin Headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");

    next();
});

app.use(express.static('users'));

//Setting express to use body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

//Connect to the database
mongoose.connect(process.env.MONGO_URL);
CloudManager.setEmailDetails(process.env.EMAIL_USERNAME, process.env.EMAIL_PASSWORD);

/*
    ACCOUNTS
*/
app.get('/account/verify/:code', async(req, res) => {
    const { code } = req.params;

    if (await CloudManager.verificationCodeExists(code)) {
        await CloudManager.verifiyAccount(code);

        return res.redirect('https://basic-web-chat.herokuapp.com/');
    }

    return res.json({
        success: false,
        message: 'Invalid verify code.'
    });
});

app.post('/api/account/auth', async(req, res) => {
    const { username, password } = req.body;

    if (await CloudManager.accountExists(username) == false) {
        return res.json({
            success: false,
            message: 'Email doesnt exist'
        });
    }

    if (await CloudManager.authenicateAccount(username, password)) {
        return res.json({
            success: true,
            message: 'You have logged in successfully',
            key: await CloudManager.getApiKey(username),
            image: await CloudManager.getProfileImage(username),
            displayName: await CloudManager.getDisplayName(username),
            userId: await CloudManager.getUserId(await CloudManager.getApiKey(username))
        });
    }

    return res.json({
        success: false,
        message: 'Invalid username or password.'
    });
});

app.post('/api/account/register', async(req, res) => {
    var { displayName, email, password, profileImage } = req.body;
    email = email.toLowerCase();

    //Validate input fields
    if (displayName == '' || email == '') {
        return res.json({
            success: false,
            message: 'Please fill in all the fields.'
        });
    }

    if (!email.includes('@') || !email.includes('.')) {
        return res.json({
            success: false,
            message: 'Please provide a valid email address.'
        });
    }

    if (password.length < 7) {
        return res.json({
            success: false,
            message: 'Passwords must be greater than 7 chars long.'
        });
    }

    if (await CloudManager.accountExists(email)) {
        return res.json({
            success: false,
            message: 'Email already in use!'
        });
    }

    const verfCode = CloudManager.hash(String(new Date().getTime()), displayName + email);
    const apiKey = CloudManager.hash(email, email + '.' + password);
    const userId = displayName + '.' + String(new Date().getTime());

    if (await CloudManager.registerAccount(userId, apiKey, displayName, email, password, profileImage, verfCode)) {
        CloudManager.sendEmail(email, "Web Chat - Account Verification", `
        Hello, ${displayName}
        Please verify you account via the following link: 
        https://basic-web-chat-api.herokuapp.com/account/verify/${verfCode}
    
        Thanks,
        Web Chat Team
        `);

        fs.writeFile(`./users/${userId}`, profileImage, 'base64', function(err) {
            console.log(err);
        });

        return res.json({
            success: true,
            message: 'Your account has been created! Please verify your account.'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to create the account.'
    });
});

/*
    SERVER MANAGMENT
*/
app.get('/api/server/manage', async(req, res) => {
    const { key } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key!'
        });
    }

    return res.json({
        success: true,
        result: (await CloudManager.getUserServerListData(key))
    });
});

app.get('/api/server/permission', async(req, res) => {
    const { key, serverId } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key!'
        });
    }

    if (await CloudManager.serverIdExists(serverId) == false) {
        return res.json({
            success: false
        });
    }

    return res.json({
        success: await CloudManager.userInServer(key, serverId)
    });
});

app.post('/api/server/manage', async(req, res) => {
    const { name, icon, key } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key!'
        });
    }

    if (name == undefined || name == '') {
        return res.json({
            success: false,
            message: 'Please provide a server name!'
        });
    }

    if (icon == undefined || icon == '') {
        return res.json({
            success: false,
            message: 'Please upload an image!'
        });
    }

    const result = await CloudManager.createServer((await CloudManager.getAccountEmail(key)), name, icon);
    if (result != undefined) {
        await CloudManager.addUserToServer((await CloudManager.getAccountEmail(key)), result);
        return res.json({
            success: true,
            message: 'Server has been created successfully!',
            id: result
        });
    }

    return res.json({
        success: false,
        message: 'Failed to create server.'
    });
});

app.put('/api/server/manage', async(req, res) => {
    const { id, key } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key!'
        });
    }

    if (await CloudManager.serverIdExists(id) == false) {
        return res.json({
            success: false,
            message: 'Invalid server id.'
        });
    }

    if (await CloudManager.userInServer(key, id)) {
        return res.json({
            success: false,
            message: 'You are already a part of this server.'
        });
    }

    if (await CloudManager.addUserToServer((await CloudManager.getAccountEmail(key)), id)) {
        return res.json({
            success: true,
            message: 'You have successfully joined the server!'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to join server.'
    });
});

app.delete('/api/server/manage', async(req, res) => {
    const { id, key } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key!'
        });
    }

    if (await CloudManager.serverIdExists(id) == false) {
        return res.json({
            success: false,
            message: 'Invalid server id.'
        });
    }

    if (await CloudManager.delUserFromServer((await CloudManager.getAccountEmail(key)), id)) {
        return res.json({
            success: true,
            message: 'You have successfully left the server!'
        });
    }

    return res.json({
        success: false,
        message: 'Failed to leave server.'
    });
});

/*
    MESSAGES
*/
app.get('/api/server/messages', async(req, res) => {
    const { serverId, key, page } = req.query;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key!'
        });
    }

    if (await CloudManager.serverIdExists(serverId) == false) {
        return res.json({
            success: false,
            message: 'Invalid server id!'
        });
    }

    return res.json({
        success: true,
        result: await CloudManager.getMessages(serverId, Number(page))
    });
});

app.post('/api/server/messages', async(req, res) => {
    const { serverId, key, message } = req.body;

    if (await CloudManager.apiKeyExists(key) == false) {
        return res.json({
            success: false,
            message: 'Invalid api key!'
        });
    }

    if (await CloudManager.addMessage(serverId, key, message)) {
        return res.json({
            success: true,
            message
        });
    }

    return res.json({
        success: false,
        message: 'Failed to send message.'
    });
});

//Error 404
// app.get('*', (req, res) => {
//     res.json({ status: false });
// });

//Web Listener\\
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});