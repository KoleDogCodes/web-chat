const Accounts = require('./models/user');
const Servers = require('./models/groups');
const Messages = require('./models/message');
const crypto = require('crypto');
const mailer = require('nodemailer');
var email, emailPassword;

module.exports = {

    //Misc
    hash: function(secert, value) {
        var hash = crypto.createHmac('sha256', secert)
            .update(value)
            .digest('hex');

        if (hash.length > 256) hash = hash.substr(0, 255);

        return hash;
    },

    setEmailDetails: function(username, password) {
        email = username;
        emailPassword = password;
    },

    sendEmail: function(to, subject, text) {
        var transporter = mailer.createTransport({
            service: 'gmail',
            auth: {
                user: email,
                pass: emailPassword
            }
        });

        var mailOptions = {
            from: email,
            to: to,
            subject: subject,
            text: text
        };

        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
                return false;
            } else {
                return true;
            }
        });
    },

    //Account Management
    verificationCodeExists: async function(code) {
        var promise = Accounts.findOne({ verified: code });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    verifiyAccount: async function(code) {
        var promise = Accounts.findOne({ verified: code }, (err, document) => {
            document.verified = '';

            document.save((err) => {
                if (err) { console.log(err); }
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;

    },

    addUserToServer: async function(email, serverId) {
        var promise = Accounts.findOne({ email }, (err, document) => {
            const servers = document.servers == undefined ? {} : JSON.parse(document.servers);
            servers[String(Object.keys(servers).length)] = serverId;

            document.servers = JSON.stringify(servers);

            document.save((err) => {
                if (err) { console.log(err); }
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    delUserFromServer: async function(email, serverId) {
        var promise = Accounts.findOne({ email }, (err, document) => {
            const servers = document.servers == undefined ? {} : JSON.parse(document.servers);

            const keys = Object.keys(servers);
            var keyToDel = undefined;

            for (var i = 0; i < keys.length; i++) {
                if (servers[keys[i]] == serverId) {
                    keyToDel = keys[i];
                    break;
                }
            }

            if (keyToDel != undefined) {
                delete servers[keyToDel];
            }

            document.servers = JSON.stringify(servers);

            document.save((err) => {
                if (err) { console.log(err); }
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    userInServer: async function(key, serverId) {
        var promise = Accounts.findOne({ apiKey: key });

        const document = await promise;

        const servers = document.servers == undefined ? {} : JSON.parse(document.servers);

        const keys = Object.keys(servers);
        var keyToDel = undefined;

        for (var i = 0; i < keys.length; i++) {
            if (servers[keys[i]] == serverId) {
                keyToDel = keys[i];
                break;
            }
        }

        if (keyToDel != undefined) {
            return true;
        }

        return false;
    },

    authenicateAccount: async function(email, password) {
        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        if (promiseValue.password == this.hash(email.toLowerCase(), password) && promiseValue.verified == '') {
            return true;
        }

        return false;
    },

    registerAccount: async function(userId, apiKey, dispName, email, password, image, verfCode) {
        email = email.toLowerCase();

        const newAccount = new Accounts({
            apiKey,
            userId,
            displayName: dispName,
            email,
            password: this.hash(email, password),
            profileImage: image,
            verified: verfCode,
            servers: JSON.stringify({})
        });

        var promise = newAccount.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    apiKeyExists: async function(key) {

        var promise = Accounts.findOne({ apiKey: key });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getApiKey: async function(email) {
        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue.apiKey;
    },

    getUserServerListData: async function(key) {
        var promise = Accounts.findOne({ apiKey: key });

        const promiseValue = await promise;

        const servers = JSON.parse(promiseValue.servers);
        const keys = Object.keys(servers);

        const serverData = {};

        for (var i = 0; i < keys.length; i++) {
            const idx = keys[i];
            serverData[i] = await this.getServerData(servers[idx]);
        }

        return serverData;
    },

    getDisplayName: async function(email) {
        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue.displayName;
    },

    getUserId: async function(key) {
        var promise = Accounts.findOne({ apiKey: key });

        const promiseValue = await promise;

        return promiseValue.userId;
    },

    accountExists: async function(email) {

        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getProfileImage: async function(email) {
        var promise = Accounts.findOne({ email: email.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue.profileImage;
    },

    getAccountEmail: async function(key) {
        var promise = Accounts.findOne({ apiKey: key });

        const promiseValue = await promise;
        return promiseValue.email;
    },

    //Group Management
    serverIdExists: async function(id) {

        var promise = Servers.findOne({ serverId: id });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    createServer: async function(owner, title, icon) {
        const serverId = this.hash(owner + new Date().getTime + title, title);

        const newServer = new Servers({
            serverId,
            title,
            owner,
            misc: JSON.stringify({
                icon: icon
            })
        });

        var promise = newServer.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? serverId : undefined;
    },

    addMessage: async function(serverId, owner, text) {
        const newMessage = new Messages({
            serverId,
            userId: await this.getUserId(owner),
            message: text,
            sent_at: new Date().getTime(),
            misc: JSON.stringify({})
        });

        var promise = newMessage.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    getServerData: async function(id) {
        var promise = Servers.findOne({ serverId: id });

        const promiseValue = await promise;

        return {
            id,
            title: promiseValue.title,
            img: JSON.parse(promiseValue.misc).icon,
            socketId: promiseValue['_id']
        };
    },

    getMessages: async function(serverId, page) {
        const pageSize = 50;
        var promise = Messages.find({ serverId }).sort({_id: -1}).limit(pageSize).skip(page * pageSize);

        const promiseValue = await promise;
        return promiseValue;
    },


}