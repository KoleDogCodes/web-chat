const mongoose = require('mongoose');

module.exports = mongoose.model('Messages', new mongoose.Schema({
    serverId: String,
    userId: String,
    message: String,
    sent_at: String,
    misc: String
}));