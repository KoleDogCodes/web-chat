const mongoose = require('mongoose');

module.exports = mongoose.model('Server', new mongoose.Schema({
    serverId: String,
    title: String,
    owner: String,
    misc: String
}));