const mongoose = require('mongoose');

module.exports = mongoose.model('Accounts', new mongoose.Schema({
    apiKey: String,
    userId: String,
    displayName: String,
    email: String,
    password: String,
    profileImage: String,
    verified: String,
    servers: String
}));