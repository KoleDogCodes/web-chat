var app = require('express')();
const cors = require('cors');

app.use(cors());

const server = require('http').Server(app);
const io = require('socket.io')(server);

const port = process.env.PORT || 3000;

console.log("Server will be listening on port " + port);
server.listen(port);

function emit(io, namespace, room, data) {
    io.of('/' + namespace).emit(room, JSON.stringify(data));
}

app.get('/*', function(req, res) {
    return res.json({
        success: true,
        message: 'Successfully connected to the socket server.'
    })
});

io.on("connection", function(socket) {

    //Client Ping Server
    socket.on("connection_status", function(data) {
        socket.emit("connection_status", "Pong");
    });

    socket.on("forward_message", function(data) {
        const jsonData = JSON.parse(data);
        io.of('/').emit(jsonData.socketId, JSON.stringify(jsonData));
    });


    socket.on('disconnect', async function() {
        const api_key = socket.handshake.headers.api_key;
        const device_id = socket.handshake.headers.device_id;

        if (api_key == undefined || device_id == undefined || api_key == null || device_id == null) return;

    });
});