const express = require('express');
const app = express();

const port = process.env.PORT || 5000;
const base = `${__dirname}/public`;

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, REQUEST");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");

    next();
});

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(`${base}/login.html`);
});


app.get('/register', (req, res) => {
    res.sendFile(`${base}/register.html`);
});

app.get('/chats/:serverId', (req, res) => {
    res.sendFile(`${base}/dashboard.html`);
});


//Web Listener\\
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});