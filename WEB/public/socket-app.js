//const SOCKET_URL = 'http://localhost:3000';
const SOCKET_URL = 'http://basic-web-chat-socket.herokuapp.com';

const socket = io(SOCKET_URL);
var serverList = [];

function pingSocketServer(list) {
    serverList = list;
    console.log(list);
    $("#loading-text").text("Pinging socket server...");
    socket.emit('connection_status', 'Ping');
}

function startListener() {
    Object.keys(serverList).forEach(id => {

        socket.on(serverList[id], function(data) {
            if (id != serverId) {

                if ($(`#${id}-badge`).text() == '') {
                    $(`#${id}-badge`).text('1');
                } else {
                    $(`#${id}-badge`).text(Number($(`#${id}-badge`).text()) + 1);
                }

                return;
            }

            appendMessage(data);
        });

    });
}

function appendMessage(data) {
    const messageData = JSON.parse(data);
    if (serverId == messageData.serverId) {
        $("#chat-messages").append(`
        <tr>
            <td>
                <div class="row">
                    <img src="${API_URL.toString().replace(/\/api/g, '')}/${messageData.userId}" class="profile-user-icon">
                </div> 
            </td>
    
            <td>
                <h6 style="color: white;" class="my-auto">${messageData.displayName}</h6>
                <p style="color: white;" class="my-auto">
                    ${messageData.message}
                </p>
            </td>
        </tr>
        `);
    }
}

socket.on('connection_status', function(data) {
    if (data == "Pong") {
        showPage();
        startListener();
    }
});




//Send Message Event
$('#send-message-button').on('keyup', (e) => {
    if (e.keyCode === 13) {
        const message = $('#send-message-button').val();

        $.post(`${API_URL}/server/messages`, { serverId, key: localStorage.getItem('apiKey'), message }).then((response) => {
            if (response.success == false) {
                Swal.fire('Error', response.message, 'warning');
                return;
            }

            socket.emit('forward_message', JSON.stringify({
                message,
                serverId,
                socketId: serverList[serverId],
                displayName: localStorage.getItem('displayName'),
                userId: localStorage.getItem('userId')
            }));
        }).fail(((xhr, textStatus, errorThrown) => {
            Swal.fire('Error 404', 'Failed to contact servers. Please try again later!', 'error');
        }));
    }
});